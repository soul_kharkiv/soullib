# SoulLib Frontend

## Install Node.js

Ensure Node.js is installed on your system.

## How to init

1. Clone the repository
2. Change directory to the repo
3. Install dependencies. Run `npm install`
4. Install recommended extensions for the VSCode workspace. Go to **Extensions** > search for `@recommended`, or run the `Extensions: Show Recommended Extensions` command.

## Scripts

An example of usage: `npm run fix:css`.

The `package.json` file includes the following scripts:

* `lint:css` - Lints all `.css` files in the css directory to check for style errors and adherence ensures to the Stylelint configuration. It lists any issues found in the CSS files.
* `fix:css` - Automatically fixes linting errors in `.css` files wherever possible. It modifies the CSS files to resolve errors.
* `format:css` - Formats all `.css` files in the CSS directory to ensure consistent styling based on the Biome configuration. It reformats the files (e.g., fixing indentation, spacing, and line breaks) without changing the logic or structure.
