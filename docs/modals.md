# Modals

## About

- Modals are positioned over everything else in the document and disable scroll from the `<body>` so that modal content scrolls instead if it is long enough.
- Only one modal window supports at a time.
- Modals show and hide animation are supported.

## Behave

### Screen positioning

- On the desktop, modals are positioned in the middle of the browser window.
- On mobile devices, specifically smartphones, modals are stuck to the bottom of the screen (browser window) and stretch to fill the screen width. It is about the viewport in portrait orientation and I have no clue how modals behave in landscape orientation.

### How to close

- Clicking on the modal 'backdrop' also known as 'overlay' will automatically close the modal.
- Pressing the `Esc` key on the keyboard will do the same.
- Clicking on the modal close button `X` will do the same too.

### Animation

- On the desktop, fade in/fade out effect with a little scaling of a modal window.
- On mobile devices, fade in/fade out effect with a little shifting by the vertical axis.

## Configuration

|Parameter|Type||Description|
|--|--|--|--|
|id|String|Required|Unique modal name. The value of this attribute in the HTML tag must be equal the id of the modal window selector, which needs to be open.|
|options|Object|Optional|Configuration object. Contain callback functions. For details, see Configuration sections: Options and Callback functions.|

### Options

Modals supported the next options.

|Parameter|Type|Default|Description|
|--|--|--|--|
|catchFocus|boolean|true|If true – when a modal window is open, the focus will loop on the active elements inside the modal. When the window closes, focus returning on the previous selector.|
|saveScrollPosition|boolean|false|If true and a modal window is high – the modal window will open at the same scroll position where it was closed. If false – it will be opened from the beginning.|

### Callback functions

Modals supported four callback functions.

|Parameter|Type|Default|Description|
|--|--|--|--|
|beforeOpen|function|Empty function|Callback function. Run before the modal is opened. The modal window object is passed to the function as context.|
|afterOpen|function|Empty function|Callback function. Run after the modal is opened. The modal window object is passed to the function as context.|
|beforeClose|function|Empty function|Callback function. Run before the modal is closed. The modal window object of the closed window is passed to the function as context.|
|afterClose|function|Empty function|Callback function. Run after the modal is closed. The modal window object of the closed window is passed to the function as context.|

## How to add a new modal

This documentation section is in process.
