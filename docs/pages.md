# Pages

## Active page

For an existing link in the navigation menu to the current (open) page, add the class `.current-page` to that link.
