# Article

## Supported features

* Headers (5 levels)
* Plain text
* Lists
  * Unordered (3 levels)
  * Ordered (max level is infinite?)
* Illustrations (pictures) with captions (optional)
* Links
  * Internal
  * External
* Code
  * Inline
  * Block
* Tooltips
* Tables with captions (optional)
* Block quotes
* Horizontal lines

## Notes

### Articles & Series linkage

For a single article (not one of the existing series of articles or a single article in a series), add the class `single-page` to the `main` element.

For the first article in a series (at least two articles), add the class `first-page` to the `main` element.

For the last article in a series (at least two articles), add the class `last-page` to the `main` element.

### Articles & Series navigation

The side panel on the article page contains a series navigation by articles and navigation by headers for the current (opened) article.
For a single article (not one of the existing series) side panel contains only navigation by headers for the article.

Series with articles and article navigation use different side panel headers. For example, `Оглавление` and `Содержание` respectively.

For the current article item in the list of contents, add the class `current-article` to the `a` element.

#### Tree

This documentation section is in process.

### Code

Syntax highlighting uses [Prism](//prismjs.com/) highlighter.

For marking up code use `<code>` on its own for inline code, or inside a `<pre>` for blocks of code.

Supported languages:

* C-like:
  * C
  * C++
* Bash
* Shell

Brackets are necessary for names of functions to highlight them in inline code snippets.

#### Languages

> According to the HTML5 spec, the recommended way to define a code language is a `language-xxxx` class, which is what Prism uses.

Block code snippets are done like this:

```
<pre><code class="language-cpp">
int main() {
  return 0;
}
</code></pre>
```

Inline code snippets example:

```
<code class="language-cpp">int main()</code>
```

Prism features:
> The `language-xxxx` class is inherited. This means that if multiple code snippets have the same language, you can just define it once, in one of their common ancestors.
>
> If you want to opt-out of highlighting a `<code>` element that inherits its language, you can add the `language-none` class to it. The none language can also be inherited to disable highlighting for the element with the class and all of its descendants.
>
> If you want to opt-out of highlighting but still use plugins like Show Invisibles, add use `language-plain` class instead.
