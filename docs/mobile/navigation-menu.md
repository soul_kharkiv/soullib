# Navigation Menu (Mobile)

## Toggle elements

For the navigation menu toggle element, add the following classes:

* `.screen-menu-toggle`
* `.no-display`

Also, set the *aria-hidden* attribute value with `true`.

## Menu

For the navigation menu in the mobile view HTML element, add the class `.screen-menu`.
