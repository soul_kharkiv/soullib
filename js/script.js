const isMobile = 'ontouchstart' in document.documentElement && navigator.userAgent.match(/Mobi/);

const Scroll = {
	scrollWidth: 0,
	scrollPosition: 0,

	init() {
		this.scrollWidth = this.getWidth();
	},

	freeze() {
		this.savePosition();

		document.body.classList.add('freeze-container');
	},

	unfreeze() {
		document.body.classList.remove('freeze-container');

		this.restorePosition();
	},

	disable(layerHasScroll = false) {
		this.savePosition();

		if (!this.scrollWidth) {
			return;
		}

		if (this.windowHasScroll() && !isMobile) {
			if (layerHasScroll) {
				document.body.style.paddingRight = `${this.scrollWidth}px`;
			} else {
				document.body.style.overflowY = 'scroll';
			}
		}
	},

	enable() {
		this.restorePosition();

		if (!this.scrollWidth) {
			return;
		}

		document.body.style.paddingRight = '';
		document.body.style.overflowY = '';
	},

	resize(layerHasScroll = false) {
		if (!this.scrollWidth) {
			return;
		}

		if (this.windowHasScroll() && !isMobile) {
			if (layerHasScroll) {
				document.body.style.overflowY = '';
				document.body.style.paddingRight = `${this.scrollWidth}px`;
			} else {
				document.body.style.overflowY = 'scroll';
				document.body.style.paddingRight = '';
			}
		}
	},

	setPosition(offset) {
		this.scrollPosition = offset;
		document.body.style.top = `-${this.scrollPosition}px`;
	},

	savePosition() {
		this.scrollPosition = window.scrollY;
		document.body.style.top = `-${this.scrollPosition}px`;
	},

	restorePosition() {
		window.scrollTo(0, this.scrollPosition);
		document.body.style.top = '';
	},

	getWidth() {
		const outer = document.createElement('div');
		outer.style.visibility = 'hidden';
		outer.style.overflow = 'scroll';
		outer.style.msOverflowStyle = 'scrollbar';
		document.body.appendChild(outer);

		const inner = document.createElement('div');
		outer.appendChild(inner);

		const scrollbarWidth = outer.offsetWidth - inner.offsetWidth;

		outer.parentNode.removeChild(outer);

		return scrollbarWidth;
	},

	windowHasScroll() {
		return document.body.scrollHeight > document.documentElement.clientHeight;
	},
};

const PageScrolling = {
	scrollBtn: {},

	lastPosition: -1,

	turningPoint: 400,

	isUp: false,

	init() {
		PageScrolling.scrollBtn = document.getElementById('scroll_btn');
		PageScrolling.scrollBtn.addEventListener('click', PageScrolling.scrollTo);
		PageScrolling.scrollBtn.classList.remove('hidden');

		window.addEventListener('scroll', () => {
			if (window.scrollY < PageScrolling.turningPoint && PageScrolling.lastPosition > PageScrolling.turningPoint) {
				PageScrolling.scrollBtn.classList.add('active');
				PageScrolling.isUp = true;
			} else {
				PageScrolling.scrollBtn.classList.remove('active');
				PageScrolling.isUp = false;
			}
		});
	},

	scrollTo(event) {
		let scrollPosition = 0;



		if (window.scrollY < PageScrolling.turningPoint && PageScrolling.lastPosition < PageScrolling.turningPoint) {
			PageScrolling.lastPosition = 0;
			PageScrolling.isUp = true;
		} else if (PageScrolling.isUp) {
			scrollPosition = PageScrolling.lastPosition;
			PageScrolling.lastPosition = 0;
			PageScrolling.isUp = false;
		} else {
			PageScrolling.lastPosition = window.scrollY;
			PageScrolling.isUp = true;
		}


		window.scrollTo({
			top: scrollPosition,
			behavior: 'smooth',
		});
	}
}

/**
 * Enum for color schemes.
 * @readonly
 * @enum {{name: String}}
 */
const ColorScheme = Object.freeze({
	Light: {name: 'light'},
	Dark: {name: 'dark'},
	Sepia: {name: 'sepia'},
	getByName(name) {
		if (name === this.Dark.name) {
			return ColorScheme.Dark;
		}
		if (name === this.Sepia.name) {
			return ColorScheme.Sepia;
		}

		return ColorScheme.Light;
	},
});

/**
 * Enum for font types.
 * @readonly
 * @enum {{name: String}}
 */
const FontType = Object.freeze({
	Serif: {name: 'serif'},
	SansSerif: {name: 'sans-serif'},
	getByName(name) {
		if (name === this.Serif.name) {
			return FontType.Serif;
		}

		return FontType.SansSerif;
	},
});

class Modal {
	static isOpened = false;

	static listOfModals = new Map();

	constructor(id, options) {
		const defaultConfigs = {
			catchFocus: true,
			saveScrollPosition: true,
			beforeOpen: () => {},
			afterOpen: () => {},
			beforeClose: () => {},
			afterClose: () => {},
		};

		this.configs = Object.assign(defaultConfigs, options);

		this.init(id);

		window.addEventListener('resize', Modal.resizeScroll);
	}

	init(id) {
		this.window = document.querySelector(`.modal-window#${id}`);
		this.wrapper = this.window.closest('.modal-wrapper');
		this.backdrop = this.window.closest('.modal-dialog');
		this.title = this.window.querySelector('.modal-title');
		this.content = this.window.querySelector('.modal-content');

		let parentNode;
		try {
			this.container = this.window.closest('.modal-container');
			if (!this.container) {
				throw new Error();
			}
			parentNode = this.container;
		} catch (e) {
			parentNode = document.body;
		} finally {
			const existingOverlay = parentNode.querySelector('.modal-overlay');
			if (existingOverlay) {
				this.overlay = existingOverlay;
			} else {
				this.overlay = document.createElement('div');
				this.overlay.classList.add('modal-overlay');
				parentNode.appendChild(this.overlay);
			}
		}

		this.starterElement = false;
		this.focusElements = [
			'a[href]',
			'input:not([disabled]):not([type="hidden"]):not([aria-hidden])',
			'select:not([disabled]):not([aria-hidden])',
			'textarea:not([disabled]):not([aria-hidden])',
			'button:not([disabled]):not([aria-hidden])',
			'[tabindex]:not([tabindex^="-"])',
		];

		this.closeAfterTransition = this.closeAfterTransition.bind(this);
		this.focusControl = this.focusControl.bind(this);
		this.focusCatcher = this.focusCatcher.bind(this);

		for (const f in this.configs) {
			if (typeof this.configs[f] === 'function') {
				this.configs[f] = this.configs[f].bind(this);
			}
		}

		Modal.listOfModals.set(id, this);
	}

	hasScroll() {
		return this.backdrop.scrollHeight > document.documentElement.clientHeight;
	}

	static open(event) {
		event.preventDefault();

		Modal.enable(event.target);
	}

	static close(event) {
		if (!Modal.isOpened) {
			return;
		}

		Modal.disable(event.target);
	}

	static definitiveClose() {
		document.body.classList.remove('modal--opened');
		Scroll.enable();
		Modal.isOpened = false;
	}

	static onKeyDown(event) {
		if ((event.key === 'Escape' || event.key === 'Esc' || event.keyCode === 27) && Modal.isOpened) {
			Modal.close(event);
		}

		if (event.keyCode === 9 && Modal.isOpened) {
			if (Modal.current.configs.catchFocus) {
				Modal.current.focusCatcher(event);
			}
		}
	}

	static onOverlay(event) {
		if (event.target.classList.contains('modal-overlay--close')) {
			Modal.close(event);
		}
	}

	static enable(target) {
		const starterElement = target.closest('.data-modal');
		Modal.current = Modal.listOfModals.get(starterElement.dataset.modal);

		if (!Modal.current) {
			Modal.definitiveClose();
			return;
		}

		Modal.current.target = target.closest('.data-modal');
		Modal.current.starterElement = starterElement;

		Scroll.disable(Modal.current.hasScroll());
		document.body.classList.add('modal--opened');

		Modal.current.configs.beforeOpen();
		Modal.current.show();

		if (Modal.current.configs.catchFocus) {
			Modal.current.focusControl();
		}

		Modal.isOpened = true;

		Modal.current.configs.afterOpen();
	}

	static disable(target) {
		Modal.current.configs.beforeClose();
		Modal.current.hide();

		Modal.current.target = null;
		Modal.current = null;
	}

	show() {
		this.overlay.classList.add('modal--active');
		this.backdrop.addEventListener('click', Modal.onOverlay, false);

		this.wrapper.classList.add('modal--active');
		this.wrapper.setAttribute('aria-hidden', 'false');
		if (this.container) {
			this.container.classList.add('modal--active');
			this.container.setAttribute('aria-hidden', 'false');
		}
	}

	hide() {
		if (this.container) {
			this.container.classList.add('modal--move');
			this.container.classList.remove('modal--active');
		}

		this.wrapper.classList.add('modal--move');
		this.wrapper.addEventListener('transitionend', this.closeAfterTransition);
		this.wrapper.classList.remove('modal--active');

		this.backdrop.removeEventListener('click', Modal.onOverlay);
		this.overlay.classList.remove('modal--active');
	}

	closeAfterTransition(event) {
		this.wrapper.classList.remove('modal--move');
		this.wrapper.removeEventListener('transitionend', this.closeAfterTransition);

		if (this.container) {
			this.container.classList.remove('modal--move');
			this.container.setAttribute('aria-hidden', 'true');
		}

		this.wrapper.setAttribute('aria-hidden', 'true');

		if (this.configs.catchFocus) {
			this.focusControl();
		}

		Modal.definitiveClose();

		if (!this.configs.saveScrollPosition) {
			window.scrollTo({
				top: 0,
				behavior: 'auto',
			});
		}

		this.configs.afterClose();
	}

	focusControl() {
		const nodes = this.window.querySelectorAll(this.focusElements);

		if (Modal.isOpened && this.starterElement) {
			this.starterElement.focus();
		} else if (nodes.length) {
			nodes[0].focus()
		};
	}

	focusCatcher(event) {
		const nodes = this.window.querySelectorAll(this.focusElements);
		const nodesArray = Array.prototype.slice.call(nodes);

		if (!this.window.contains(document.activeElement)) {
			nodesArray[0].focus();
			event.preventDefault();
		} else {
			const focusedItemIndex = nodesArray.indexOf(document.activeElement);

			if (event.shiftKey && focusedItemIndex === 0) {
				nodesArray[nodesArray.length - 1].focus();
				event.preventDefault();
			}

			if (!event.shiftKey && focusedItemIndex === nodesArray.length - 1) {
				nodesArray[0].focus();
				event.preventDefault();
			}
		}
	}

	static addListeners() {
		const openButtons = document.querySelectorAll('.modal-open');
		for (const btn of openButtons) {
			btn.addEventListener('click', Modal.open, false);
		}

		const closeButtons = document.querySelectorAll('.modal-close');
		for (const btn of closeButtons) {
			btn.addEventListener('click', Modal.close, false);
		}

		document.addEventListener('keydown', Modal.onKeyDown, false);
	}

	static resizeScroll(event) {
		if (Modal.isOpened) {
			Scroll.resize(Modal.current.hasScroll());
		}
	}
}

const DefaultSettings = {
	fontType: FontType.SansSerif,
	fontSize: 16,
	colorScheme: ColorScheme.Light,
	hasBgImage: false,
	bgImage: 0,
	interfaceOpacity: 100,

	minFontSize: 10,
	maxFontSize: 32,
	minInterfaceOpacity: 75,
	maxInterfaceOpacity: 100,
	codeOpacity: 65,
};

function validate(prop, value) {
	let val = value;

	if (prop === 'fontSize') {
		val = Number.parseInt(value, 10);
		if (value > DefaultSettings.maxFontSize) {
			val = DefaultSettings.maxFontSize;
		} else if (value < DefaultSettings.minFontSize) {
			val = DefaultSettings.minFontSize;
		}
	} else if (prop === 'interfaceOpacity') {
		val = Number.parseInt(value, 10);
		if (value > DefaultSettings.maxInterfaceOpacity) {
			val = DefaultSettings.maxInterfaceOpacity;
		} else if (value < DefaultSettings.minInterfaceOpacity) {
			val = DefaultSettings.minInterfaceOpacity;
		}
	} else if (prop === 'fontType' && typeof value === 'string') {
		val = FontType.getByName(value);
	} else if (prop === 'colorScheme' && typeof value === 'string') {
		val = ColorScheme.getByName(value);
	} else if (prop === 'hasBgImage' && typeof value === 'string') {
		val = value === 'true';
	}

	return val;
}

function stringify(value) {
	let string;

	switch (typeof value) {
		case 'number':
			string = value.toString();
			break;

		case 'object':
			string = value.name;
			break;

		// biome-ignore lint/complexity/noUselessSwitchCase: <Display all main process types>
		case 'string': /* fallthrough */
		default:
			string = value;
			break;
	}

	return string;
}

let Settings = {
	fontType: DefaultSettings.fontType,
	fontSize: DefaultSettings.fontSize,
	colorScheme: DefaultSettings.colorScheme,
	hasBgImage: DefaultSettings.hasBgImage,
	bgImage: DefaultSettings.bgImage,
	interfaceOpacity: DefaultSettings.interfaceOpacity,
};

Settings = new Proxy(Settings, {
	set(obj, prop, value) {
		let val = value;
		let setToLocalStorage = true;
		if (typeof value === 'object' && value.storage) {
			val = value.value;
			setToLocalStorage = false;
		}

		val = validate(prop, val);

		// biome-ignore lint/suspicious/noDoubleEquals: <Unknown type of val>
		if (obj[prop] != val) {
			obj[prop] = val;
			if (setToLocalStorage) {
				localStorage.setItem(prop, stringify(val));
			}
			Page.update(prop);
		}
		SettingsUI.update(prop);

		return true;
	},

	get(obj, prop) {
		let value;

		if (prop === 'fontType' || prop === 'colorScheme') {
			value = obj[prop].name;
		} else if (prop === 'bgImage' && obj[prop] > 0) {
			const image = document.querySelector(`#article-image_${obj[prop]} + label img`);
			value = {
				id: obj[prop],
				preload: image.src,
				full: image.dataset.src,
			};
		} else {
			value = obj[prop];
		}

		return value;
	},
});

const OpacitySetting = {
	settingsItem: {},
	settingInput: {},
	settingText: {},
	opacityValue: 0,

	init() {
		this.settingsItem = document.getElementById('opacity_setting');
		this.settingInput = document.getElementById('background-opacity');
		this.settingText = document.getElementById('background-opacity-value');

		this.setData(DefaultSettings.maxInterfaceOpacity);

		this.settingInput.addEventListener('input', OpacitySetting.rangeInputHandler);
	},

	enable() {
		this.settingsItem.classList.remove('disabled');
		this.settingInput.disabled = false;
	},

	disable() {
		this.settingsItem.classList.add('disabled');
		this.settingInput.disabled = true;
	},

	setData(value) {
		this.opacityValue = value; // Set value
		this.settingText.innerHTML = value; // Set text
		this.settingInput.valueAsNumber = value; // Set range value
	},

	getValue() {
		return this.opacityValue;
	},

	rangeInputHandler(event) {
		OpacitySetting.setData(event.target.valueAsNumber);
	},
};

/* Base class for settings parameters */
class SettingsParam {
	// biome-ignore lint/complexity/noUselessConstructor: <Parent class needs a constructor>
	constructor() {}

	static getValueHandler(event) {
		console.warn('Abstract method. Should be overridden in child class.');
	}

	updateUI() {
		console.warn('Abstract method. Should be overridden in child class.');
	}
}

class FontTypeSetting extends SettingsParam {
	constructor(parent) {
		super();
		this.elements = parent.querySelectorAll("input[name='s__font-type']");
	}

	static getValueHandler(event) {
		Settings.fontType = FontType.getByName(event.target.value);
	}

	updateUI() {
		for (const radio of this.elements) {
			radio.checked = false;
			if (radio.value === Settings.fontType) {
				document.getElementById(radio.id).checked = true;
			}
		}
	}
}

class FontSizeSetting extends SettingsParam {
	constructor(parent) {
		super();
		this.element = parent.querySelector("input[name='s__font-size']");
	}

	static getValueHandler(event) {
		const value = Number.parseInt(event.target.value, 10);
		Settings.fontSize = Number.isInteger(value) ? value : DefaultSettings.fontSize;
	}

	updateUI() {
		this.element.value = Settings.fontSize;
	}
}

class ColorSchemeSetting extends SettingsParam {
	constructor(parent) {
		super();
		this.elements = parent.querySelectorAll("input[name='s__color-scheme']");
	}

	static getValueHandler(event) {
		Settings.colorScheme = ColorScheme.getByName(event.target.value);
	}

	updateUI() {
		for (const radio of this.elements) {
			radio.checked = false;
			if (radio.value === Settings.colorScheme) {
				document.getElementById(radio.id).checked = true;
			}
		}
	}
}

class BackgroundImageSetting extends SettingsParam {
	constructor(parent) {
		super();
		this.elements = parent.querySelectorAll("input[name='s__article-bg']");
	}

	static getValueHandler(event) {
		const image = event.target.value;

		BackgroundImageSetting.updateOpacityState(image);

		if (image === 'none' || Number.isNaN(Number(image))) {
			Settings.hasBgImage = false;
			Settings.bgImage = 0;
		} else {
			Settings.hasBgImage = true;
			Settings.bgImage = Number.parseInt(image, 10);
		}
	}

	updateUI() {
		let image = 'none';

		if (Settings.hasBgImage) {
			image = Settings.bgImage.id;
		}

		for (const radio of this.elements) {
			radio.checked = false;
			// biome-ignore lint/suspicious/noDoubleEquals: <Unknown type of value>
			if (radio.value == image) {
				document.getElementById(radio.id).checked = true;
			}
		}

		BackgroundImageSetting.updateOpacityState(image);
	}

	static updateOpacityState(value) {
		if (value === 'none' || Number.isNaN(Number(value))) {
			OpacitySetting.disable();
		} else {
			OpacitySetting.enable();
		}
	}
}

class InterfaceOpacitySetting extends SettingsParam {
	constructor(parent) {
		super();
		this.element = parent.querySelector("input[name='s__opacity-bg']");
	}

	static getValueHandler(event) {
		const value = Number.parseInt(event.target.value, 10);
		Settings.interfaceOpacity = Number.isInteger(value)
			? value
			: DefaultSettings.interfaceOpacity;
	}

	updateUI() {
		OpacitySetting.setData(Settings.interfaceOpacity);
	}
}

class SettingsUI {
	static modal;

	static params;

	static init() {
		OpacitySetting.init();

		SettingsUI.modal = new Modal('settings', {
			beforeOpen: SettingsUI.initValueHandlers,
			afterClose: SettingsUI.destroyValueHandlers,
		});

		SettingsUI.params = {
			fontType: new FontTypeSetting(SettingsUI.modal.content),
			fontSize: new FontSizeSetting(SettingsUI.modal.content),
			colorScheme: new ColorSchemeSetting(SettingsUI.modal.content),
			bgImage: new BackgroundImageSetting(SettingsUI.modal.content),
			interfaceOpacity: new InterfaceOpacitySetting(SettingsUI.modal.content),
		};

		if (!Page.isArticle) {
			SettingsUI.disableArticleSettings();
		}
	}

	// Context bind by Modal's init method
	static initValueHandlers() {
		// FontType
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		const fontTypes = this.content.querySelectorAll("input[name='s__font-type']");
		for (const radio of fontTypes) {
			radio.addEventListener('change', FontTypeSetting.getValueHandler);
		}

		// FontSize
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content
			.querySelector("input[name='s__font-size']")
			.addEventListener('change', FontSizeSetting.getValueHandler);

		// ColorScheme
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		const colorSchemes = this.content.querySelectorAll("input[name='s__color-scheme']");
		for (const radio of colorSchemes) {
			radio.addEventListener('change', ColorSchemeSetting.getValueHandler);
		}

		// HasBgImage & BgImage
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		const radioBgImages = this.content.querySelectorAll("input[name='s__article-bg']");
		for (const radio of radioBgImages) {
			radio.addEventListener('change', BackgroundImageSetting.getValueHandler);
		}

		// Interface Opacity
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content
			.querySelector("input[name='s__opacity-bg']")
			.addEventListener('input', InterfaceOpacitySetting.getValueHandler);
	}

	static update(prop) {
		switch (prop) {
			case 'fontType':
				SettingsUI.params.fontType.updateUI();
				break;

			case 'fontSize':
				SettingsUI.params.fontSize.updateUI();
				break;

			case 'colorScheme':
				SettingsUI.params.colorScheme.updateUI();
				break;

			case 'hasBgImage':
				break;

			case 'bgImage':
				SettingsUI.params.bgImage.updateUI();
				break;

			case 'interfaceOpacity':
				SettingsUI.params.interfaceOpacity.updateUI();
				break;

			default:
				break;
		}
	}

	static disableArticleSettings() {
		const articleSettings = SettingsUI.modal.content.querySelectorAll('.article-settings');
		for (const setting of articleSettings) {
			setting.classList.add('disabled');
		}

		const fontTypes = SettingsUI.modal.content.querySelectorAll("input[name='s__font-type']");
		for (const element of fontTypes) {
			element.disabled = true;
		}

		SettingsUI.modal.content.querySelector("input[name='s__font-size']").disabled = true;

		const fontSizeButtons = SettingsUI.modal.content.querySelectorAll('button.operator');
		for (const element of fontSizeButtons) {
			element.disabled = true;
		}
	}

	// Context bind by Modal's init method
	static destroyValueHandlers() {
		// FontType
		try {
			// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
			const fontTypes = this.content.querySelectorAll("input[name='s__font-type']");
			for (const radio of fontTypes) {
				radio.removeEventListener('change', FontTypeSetting.getValueHandler);
			}
		} catch (error) {
			console.error(`Remove font type radio buttons EventListener: ${error}`);
		}

		// FontSize
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content
			.querySelector("input[name='s__font-size']")
			.removeEventListener('change', FontSizeSetting.getValueHandler);

		// ColorScheme
		try {
			// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
			const colorSchemes = this.content.querySelectorAll("input[name='s__color-scheme']");
			for (const radio of colorSchemes) {
				radio.removeEventListener('change', ColorSchemeSetting.getValueHandler);
			}
		} catch (error) {
			console.error(`Remove color scheme radio buttons EventListener: ${error}`);
		}

		// HasBgImage & BgImage
		try {
			// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
			const radioBgImages = this.content.querySelectorAll("input[name='s__article-bg']");
			for (const radio of radioBgImages) {
				radio.removeEventListener('change', BackgroundImageSetting.getValueHandler);
			}
		} catch (error) {
			console.error(`Remove bg images to article radio buttons EventListener: ${error}`);
		}

		// Interface Opacity
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content
			.querySelector("input[name='s__opacity-bg']")
			.removeEventListener('input', InterfaceOpacitySetting.getValueHandler);
	}
}

class TooltipUI {
	static modal;

	static init() {
		TooltipUI.modal = new Modal('tooltip', {
			beforeOpen: TooltipUI.setData,
			afterClose: TooltipUI.reset,
		});
	}

	// Context bind by Modal's init method
	static setData() {
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		if (!this.target) {
			Modal.definitiveClose();
			return;
		}

		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.title.innerHTML = this.target.innerText;
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content.innerHTML = this.target.querySelector('.tooltip-content').innerHTML;
	}

	// Context bind by Modal's init method
	static reset() {
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.title.innerHTML = '';
		// biome-ignore lint/complexity/noThisInStatic: <`this` bound to the function in Modal's init method>
		this.content.innerHTML = '';
	}
}

class NavigationUI {
	static init() {
		NavigationUI.addListeners();
	}

	static addListeners() {
		const menuToggles = document.querySelectorAll('.screen-menu-toggle');
		for (const menu of menuToggles) {
			menu.addEventListener('change', NavigationUI.toggleMenu);
		}

		const menuAnchorLinks = document.querySelectorAll('.screen-menu a[href^="#"]');
		for (const link of menuAnchorLinks) {
			link.addEventListener('click', NavigationUI.execAnchorLink);
		}
	}

	static toggleMenu(event) {
		if (event.target.checked) {
			NavigationUI.current = event.target;
			Scroll.freeze();
		} else {
			Scroll.unfreeze();
			NavigationUI.current = null;
		}
	}

	static execAnchorLink(event) {
		event.preventDefault();

		const target = document.querySelector(event.target.getAttribute('href'));

		Scroll.setPosition(Math.max(target.offsetTop - 15, 0));
		NavigationUI.close();
	}

	static close() {
		const event = new Event('change', {
			bubbles: true,
		});
		NavigationUI.current.checked = false;
		NavigationUI.current.dispatchEvent(event);
	}
}

const ArticleProgress = {
	init() {
		ArticleProgress.bar = document.getElementById('article_progress_bar');
		ArticleProgress.height = document.documentElement.scrollHeight - document.documentElement.clientHeight;

		window.addEventListener('scroll', ArticleProgress.scrollEventHandler);
	},

	scrollEventHandler(event) {
		const scrolled = (document.documentElement.scrollTop / ArticleProgress.height) * 100;

		ArticleProgress.bar.style.width = `${scrolled}%`;
	}
}

const Tree = {
	init() {
		const trees = document.querySelectorAll('.tree');
		for (const tree of trees) {
			Tree.setupUI(tree);

			const items = tree.querySelectorAll('li');
			for (const item of items) {
				if (item.hasAttribute('aria-expanded')) {
					item.addEventListener('click', Tree.expandHandler);
				}
			}
		}
	},

	setupUI(tree) {
		const icon = document.createElement('span');
		icon.classList.add('tree-expander-icon');
		icon.setAttribute('aria-hidden', 'true');
		icon.innerHTML = `<svg><use href="../images/icons-bundle.svg#icon_arrow-right"></use></svg>`;

		tree.setAttribute('role', 'tree');

		const groups = tree.querySelectorAll('li > ul');
		for (const group of groups) {
			group.setAttribute('role', 'group');
			group.classList.add('tree-group');

			const parent = group.parentNode;
			if (parent.hasAttribute('aria-expanded') && parent.getAttribute('aria-expanded')) {
				parent.classList.add('is-expanded');
			} else {
				parent.setAttribute('aria-expanded', 'false');
			}

			const expander = parent.querySelector('span');
			if (expander) {
				expander.classList.add('tree-expander');
				expander.prepend(icon);
			}
		}

		const items = tree.querySelectorAll('li');
		for (const item of items) {
			item.setAttribute('role', 'treeitem');
			item.classList.add('tree-item');

			if (item.hasAttribute('aria-expanded')) {
				continue;
			}

			item.classList.add('is-leaf');
		}
	},

	expandHandler(event) {
		const item = event.target.closest('.tree-item');

		if (item.classList.contains('is-expanded')) {
			item.classList.remove('is-expanded');
			item.setAttribute('aria-expanded', 'false');
		} else {
			item.classList.add('is-expanded');
			item.setAttribute('aria-expanded', 'true');
		}
	}
}

class Page {
	static article = document.getElementById('article');

	static aside = document.getElementById('aside');

	static pageElements = document.querySelectorAll('.page-element');

	static background = {
		container: document.getElementById('bg-container'),
		preview: document.getElementById('bg-preview'),
		placeholder: document.getElementById('bg-placeholder'),
		image: document.getElementById('bg-image'),
	};

	static codeBlocks;

	static isArticle;

	static init() {
		Page.isArticle = document.querySelector('main').classList.contains('article-page');

		Page.codeBlocks = document.querySelectorAll(
			':not(pre) > code[class*="language-"], pre[class*="language-"]'
		);

		PageScrolling.init();
		NavigationUI.init();
		Tree.init();

		if (Page.isArticle) {
			ArticleProgress.init();
		}

		const scriptsDependentElements = document.querySelectorAll('.scripted');
		for (const element of scriptsDependentElements) {
			element.classList.remove('no-display');
		}

		const noScriptElements = document.querySelectorAll('.noscript');
		for (const element of noScriptElements) {
			element.classList.add('no-display');
		}
	}

	static update(prop) {
		switch (prop) {
			case 'fontType':
				Update.fontType();
				break;

			case 'fontSize':
				Update.fontSize();
				break;

			case 'colorScheme':
				Update.colorScheme();
				break;

			case 'hasBgImage':
				if (!isMobile) { Update.hasBgImage(); }
				break;

			case 'bgImage':
				if (!isMobile) { Update.bgImage(); }
				break;

			case 'interfaceOpacity':
				if (!isMobile) { Update.interfaceOpacity(); }
				break;

			default:
				break;
		}
	}

	static enableTooltips() {
		TooltipUI.init();

		const tooltips = document.querySelectorAll('.in-text-tooltip');
		for (const element of tooltips) {
			element.querySelector('.tooltip-content').classList.add('no-display');
			element.addEventListener('click', Modal.open, false);
		}
	}

	static clearPageElementsCustomStyles() {
		for (const element of Page.pageElements) {
			element.style.backgroundColor = '';
			element.style.borderColor = '';
		}
	}
}

class BackgroundColor {
	constructor(element) {
		this.element = element;
	}

	getRGB() {
		const regExp = /rgba?\(([0-9]{1,3}),\s*([0-9]{1,3}),\s*([0-9]{1,3})(.*)\)/i;
		const bgColor = getComputedStyle(this.element).backgroundColor;

		this.rgb = bgColor.match(regExp);

		return this;
	}

	setRGBA(opacity) {
		this.element.style.backgroundColor = '';

		if (!this.rgb) {
			this.getRGB();
		}

		this.element.style.backgroundColor = `rgba(${this.rgb[1]}, ${this.rgb[2]}, ${this.rgb[3]}, ${opacity / 100})`;
	}
}

class Update {
	static fontType() {
		if (Page.isArticle) {
			Page.article.classList.remove(FontType.SansSerif.name, FontType.Serif.name);
			Page.article.classList.add(Settings.fontType);

			Page.aside.classList.remove(FontType.SansSerif.name, FontType.Serif.name);

			if (isMobile) {
				return;
			}

			Page.aside.classList.add(Settings.fontType);
		}
	}

	static fontSize() {
		if (Page.isArticle) {
			Page.article.style.fontSize = `${Settings.fontSize}px`;
		}
	}

	static colorScheme() {
		document.body.classList.add('change-animation');
		document.body.classList.remove(
			ColorScheme.Light.name,
			ColorScheme.Dark.name,
			ColorScheme.Sepia.name
		);
		document.body.classList.add(Settings.colorScheme);

		Update.hasBgImage();

		setTimeout(() => {
			document.body.classList.remove('change-animation');
		}, 100);
	}

	static hasBgImage() {
		if (isMobile) {
			return;
		}

		if (Settings.hasBgImage) {
			Update.interfaceOpacity();
			if (Page.isArticle) {
				Update.codeBlocksOpacity();
			}
		} else {
			Page.clearPageElementsCustomStyles();
			if (Page.isArticle) {
				for (const element of Page.codeBlocks) {
					element.style.backgroundColor = '';
				}
			}
		}
	}

	static bgImage() {
		if (isMobile) {
			return;
		}

		if (Settings.hasBgImage) {
			Page.background.container.classList.add('visible');
			Page.background.container.classList.remove('hidden');

			Page.background.image.classList.add('hidden');
			Page.background.image.classList.remove('visible');
			setTimeout(() => {
				Page.background.preview.src = Settings.bgImage.preload;

				Page.background.placeholder.classList.add('hidden');
				Page.background.placeholder.classList.remove('visible');
				setTimeout(() => {
					Page.background.image.src = '';
					Page.background.image.src = Settings.bgImage.full;
					Page.background.image.classList.add('visible');
					Page.background.image.classList.remove('hidden');

					Page.background.placeholder.src = Settings.bgImage.preload;
					Page.background.placeholder.classList.add('visible');
					Page.background.placeholder.classList.remove('hidden');
				}, 300);
			}, 50);
		} else {
			Page.background.container.classList.add('hidden');
			Page.background.container.classList.remove('visible');
			setTimeout(() => {
				Page.background.preview.src = '';
				Page.background.placeholder.src = '';
				Page.background.image.src = '';
			}, 300);
		}
	}

	static interfaceOpacity() {
		if (isMobile) {
			return;
		}

		if (Settings.hasBgImage && Settings.interfaceOpacity < 100) {
			for (const element of Page.pageElements) {
				(new BackgroundColor(element)).setRGBA(Settings.interfaceOpacity);
				element.style.borderColor = 'transparent';
			}
		} else {
			Page.clearPageElementsCustomStyles();
		}
	}

	static codeBlocksOpacity() {
		if (isMobile) {
			return;
		}

		if (Settings.hasBgImage && Settings.interfaceOpacity < 100) {
			for (const element of Page.codeBlocks) {
				(new BackgroundColor(element)).setRGBA(DefaultSettings.codeOpacity);
			}
		} else {
			for (const element of Page.codeBlocks) {
				element.style.backgroundColor = '';
			}
		}
	}
}

const LocalStorage = {
	isAvailable() {
		const test = 'test';
		try {
			localStorage.setItem(test, test);
			localStorage.removeItem(test);
			return true;
		} catch (e) {
			console.warn('localStorage is unavailable', e);
			return false;
		}
	},

	loadData() {
		for (const name in Settings) {
			if (localStorage.getItem(name) == null) {
				Settings[name] = DefaultSettings[name];
			} else {
				Settings[name] = localStorage.getItem(name);
			}
		}
	},

	addListener() {
		window.addEventListener('storage', event => {
			if (event.newValue != null) {
				Settings[event.key] = {
					value: event.newValue,
					storage: true,
				};
			}
		});
	}
}

window.onload = () => {
	Page.init();
	Scroll.init();

	SettingsUI.init();

	if (LocalStorage.isAvailable()) {
		console.log('localStorage is available');
		LocalStorage.loadData();
		LocalStorage.addListener();
	}

	Modal.addListeners();

	if (Page.isArticle && isMobile) {
		Page.enableTooltips();
	}
};
